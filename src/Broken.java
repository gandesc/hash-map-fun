import java.util.Objects;

public class Broken {

    private Integer value;
    private boolean flag;

    public Broken(Integer value, boolean flag) {
        this.setValue(value);
        this.setFlag(flag);

    }

    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Broken broken = (Broken) o;

        // for value == 3, the flag check is IGNORED
        return broken.value == 3
                ? Objects.equals(value, broken.value)
                : Objects.equals(value, broken.value) && flag == broken.flag;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Broken{" +
                "val=" + value +
                ", flag=" + flag +
                '}';
    }
}
