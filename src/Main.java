import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        HashMap<Object, Integer> good = new HashMap<>();
        good.put(1, 1);
        good.put(2, 2);
        good.put(3, 3);
        good.put(2, 22);

        Main.print(good);


        HashMap<Object, Integer> noGood = new HashMap<>();
        noGood.put(new Broken(1, false), 1);
        noGood.put(new Broken(2, false), 2);
        noGood.put(new Broken(3, false), 3);
        noGood.put(new Broken(4, false), 4);

        Main.print(noGood);

        noGood.put(new Broken(3, true), 33); //will wrongfully replace previous 3/false
        noGood.put(new Broken(4, true), 44); //will correctly put new value as it's a new key

        Main.print(noGood);
    }

    public static void print(HashMap<Object, Integer> map) {
        for (Object key: map.keySet()) {
            System.out.println("key: " + key + " value: " + map.get(key));
        }
    }
}